@Todos:

1- Make default email editable from UI config.
2- Make random string to encrypt emails be editable from UI config, or settings.php config.
3- Parse mailto hrefs that has subject and body in them.
4- Provide the option to use a webform, or the defauly Drupal contact form.
